package GuesNumber;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

class ScannerWrapperTest {
    @Test
    public void getNumberTest() {
        ScannerWrapper wrapper = Mockito.mock(ScannerWrapper.class);
        Mockito.when(wrapper.getNumber()).thenReturn(1);
        int result = wrapper.getNumber();
        Assert.assertEquals(result, 1);
    }
    @Test
    public void getCommandTest() {
        ScannerWrapper wrapper = Mockito.mock(ScannerWrapper.class);
        Mockito.when(wrapper.getCommand()).thenReturn("Тепло");
        String expected = wrapper.getCommand();
        Assert.assertEquals(expected, "Тепло");
    }



}