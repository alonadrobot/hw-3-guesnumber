package GuesNumber;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ScannerWrapper scannerWrapper = new ScannerWrapper(new Scanner(System.in));
        DiapasonService diapasonService = new DiapasonService();
        CommandService commandService = new CommandService(scannerWrapper, diapasonService);
        while (true) {
            System.out.print("Введите команду:\n" +
                    "1  start \n" +
                    "2. help\n" +
                    "3. exit\n");
            String command = scannerWrapper.getCommand();
            switch (command) {
                case "Start":
                case "1":
                    commandService.setDiapason(diapasonService);
                    commandService.amount(diapasonService);
                    break;
                case "help":
                case "2":
                    System.out.println("При нажатии 1 или Start - запускается игра.\nПри нажатии 2 или Help - " +
                            "вызывается путеводитель по приложению.\nПри нажатии 3 или Exit - выход с приложения\n" );
                    break;
                case "exit":
                case "3":
                    System.exit(0);
                default:
                    System.out.println("Не правильно веденная команда");
                    break;
            }
        }
    }
}
