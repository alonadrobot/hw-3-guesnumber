package GuesNumber;

public class CommandService {
    private ScannerWrapper scanner;
    private DiapasonService diapasonService;

    public boolean setDiapason(DiapasonService diapasonService) {
        System.out.println("Введите минимальное число >= 1");
        int min = Integer.parseInt(scanner.getCommand());
        diapasonService.setMin(min);
        System.out.println("Введите максимально число <= 200");
        int max = Integer.parseInt(scanner.getCommand());
        diapasonService.setMax(max);

        diapasonService.setDiapason(diapasonService.getMax() - diapasonService.getMin() + 1);
        if (diapasonService.getMin() <= 1 || diapasonService.getMax() >= 200) {
            System.out.println("Число должно быть больше 1 и меньше 200\n");
            return false;
        }
        diapasonService.setArray(new boolean[diapasonService.getDiapason()]);
        return true;
    }


    public CommandService(ScannerWrapper scanner, DiapasonService diapasonService) {
        this.scanner = scanner;
        this.diapasonService = diapasonService;
    }


    public int generateRandomNumber(DiapasonService diapasonService) {
        int randomNumber;
        do {
            randomNumber = (int) (Math.random() * diapasonService.getDiapason() + diapasonService.getMin());
        } while (generatedNumber(randomNumber, diapasonService));
        return randomNumber;
    }

    private boolean generatedNumber(int number, DiapasonService diapasonService) {
        if (diapasonService.getArray()[number - diapasonService.getMin()]) {
            return true;
        } else {
            diapasonService.getArray()[number - diapasonService.getMin()] = true;
            return false;
        }
    }

    public void amount(DiapasonService diapasonService){
        System.out.println("Введите количество вашых попыток");
        int amount= scanner.getNumber();
        diapasonService.setAmount(amount);
        int count1=0,count2 = amount, randomNumber=generateRandomNumber(diapasonService);;
        boolean flag=false;
        while (!flag&&count1!=amount){
            System.out.println("Попробуйте угадать число");
            int number= scanner.getNumber();
            diapasonService.setNumber(number);
            flag=check(number,randomNumber);
            if(flag==false) {
                count2 -= 1;
                System.out.println("Вы не угадали число у вас осталось " + count2 + " количество попыток");
            }
            count1++;
        }
    }

    public boolean check(int number,int randomNumber){
        System.out.println(randomNumber);
        if(number==randomNumber){
           return true;
        }else {
            return false;
        }
    }

}